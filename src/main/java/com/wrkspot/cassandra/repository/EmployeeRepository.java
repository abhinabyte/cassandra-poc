/**
 * 
 */
package com.wrkspot.cassandra.repository;

import org.springframework.data.cassandra.repository.CassandraRepository;
import com.wrkspot.cassandra.entity.Employee;

/**
 * @author abhinab
 *
 */
public interface EmployeeRepository extends CassandraRepository<Employee, String> {

}
