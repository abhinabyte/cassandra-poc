/**
 * 
 */
package com.wrkspot.cassandra.entity;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;
import lombok.Data;

/**
 * @author abhinab
 *
 */
@Data
@Table("employee")
public class Employee {

  @PrimaryKeyColumn(name = "employee_id", type = PrimaryKeyType.PARTITIONED)
  private String employeeId;

  private String name;

}
