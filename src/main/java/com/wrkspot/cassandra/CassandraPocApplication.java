package com.wrkspot.cassandra;

import java.util.List;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.util.CollectionUtils;
import com.wrkspot.cassandra.entity.Employee;
import com.wrkspot.cassandra.repository.EmployeeRepository;

@SpringBootApplication
public class CassandraPocApplication {

  public static void main(String[] args) {
    SpringApplication.run(CassandraPocApplication.class, args);
  }

  @Bean
  public CommandLineRunner run(EmployeeRepository employeeRepository) {
    return args -> {

      while (true) {
        List<Employee> employees = employeeRepository.findAll();

        if (!CollectionUtils.isEmpty(employees)) {
          System.out.println(employees);
        }

        System.out.println("run done");

        Thread.sleep(5000);
      }
    };
  }

}
